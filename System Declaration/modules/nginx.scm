(define-module (modules nginx)
  #:use-module (gnu services web)
  #|
  The nginx service currently has three blocks.
  It is designed to route all the domains we have to https://joinjabber.org ;
  Some "gotchas" of guix or nginx:
  - If the joinjabber.org redirect block to the root location is not added there is an endless redirect.
  Because nginx is confused where the site is.
  - In this config certbot adds its own block that is basically the 80 block plus a location for well-known with server names all the domains it generates certificates for.
  That means DO NOT add any server names in the 80 block declared in the nginx service below.
  - Guix adds some default fields on top of this.
  namely on top:
  user nginx nginx;
  pid /var/run/nginx/pid;
  error_log /var/log/nginx/error.log error;
  events { }
  http { }
  client_body_temp_path /var/run/nginx/client_body_temp;
  proxy_temp_path /var/run/nginx/proxy_temp;
  fastcgi_temp_path /var/run/nginx/fastcgi_temp;
  uwsgi_temp_path /var/run/nginx/uwsgi_temp;
  scgi_temp_path /var/run/nginx/scgi_temp;
  access_log /var/log/nginx/access.log;
  include /gnu/store/fz71kvlyhzg6rq0whxr11xbxzfb18igi-nginx-1.23.3/share/nginx/conf/mime.types;
  and in server blocks:
  root /srv/http;
  index index.html;
  server_tokens off;
  |#
  #:export (joinjabber))

(define joinjabber
  (nginx-configuration
   (server-blocks
                                        ; all requests to all domains at port 80(http) go to https of their respective domain.
    (list (nginx-server-configuration
           (listen '("80" "[::]:80"))
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("return 301 https://$host$request_uri;"))))))
                                        ; the main site urls all redirect to https://joinjabber.org
          (nginx-server-configuration
           (listen '("443 ssl" "[::]:443 ssl"))
           (server-name '("www.joinjabber.org joinxmpp.org www.joinxmpp.org"))
           (ssl-certificate "/etc/letsencrypt/live/joinjabber.org/fullchain.pem")
           (ssl-certificate-key "/etc/letsencrypt/live/joinjabber.org/privkey.pem")
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("return 301 https://joinjabber.org$request_uri;"))))))
          ;; https://joinjabber.org goes to the root folder where the site content is located
          (nginx-server-configuration
           (listen '("443 ssl" "[::]:443 ssl"))
           (server-name '("joinjabber.org"))
           (ssl-certificate "/etc/letsencrypt/live/joinjabber.org/fullchain.pem")
           (ssl-certificate-key "/etc/letsencrypt/live/joinjabber.org/privkey.pem")
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("root /srv/http;")))
             ;; this regex was copied from the old server. just works for now so leave as is.
             (nginx-location-configuration
              (uri "~ ^/support/?$")
              (body '("return 302 \"https://chat.joinjabber.org/#/guest?join=support\";"))))))
          (nginx-server-configuration
           (listen '("443 ssl" "[::]:443 ssl"))
           (server-name '("invite.joinjabber.org"))
           (ssl-certificate "/etc/letsencrypt/live/joinjabber.org/fullchain.pem")
           (ssl-certificate-key "/etc/letsencrypt/live/joinjabber.org/privkey.pem")
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("root /srv/http-invite;"))))))
          (nginx-server-configuration
           (listen '("443 ssl" "[::]:443 ssl"))
           (server-name '("chat.joinjabber.org"))
           (ssl-certificate "/etc/letsencrypt/live/joinjabber.org/fullchain.pem")
           (ssl-certificate-key "/etc/letsencrypt/live/joinjabber.org/privkey.pem")
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("root /srv/http-chat;")))
             ;; these were copied from the old config and prosody docs. leave as is
             (nginx-location-configuration
              (uri "/http-bosh")
              (body '("proxy_pass http://localhost:5280/http-bind;"
                      "proxy_set_header Host $host;"
                      "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                      "proxy_set_header X-Forwarded-Proto $scheme;"
                      "proxy_buffering off;"
                      "tcp_nodelay on;")))
             (nginx-location-configuration
              (uri "/xmpp-websocket")
              (body '("proxy_pass http://localhost:5280/xmpp-websocket;"
                      "proxy_http_version 1.1;"
                      "proxy_set_header Connection \"Upgrade\";"
                      "proxy_set_header Upgrade $http_upgrade;"
                      "proxy_set_header Host $host;"
                      "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                      "proxy_set_header X-Forwarded-Proto $scheme;"
                      "proxy_read_timeout 900s;")))
             (nginx-location-configuration
              (uri "/msg")
              (body '("proxy_pass http://localhost:5280/msg;"
                      "proxy_http_version 1.1;"
                      "proxy_set_header Connection \"Upgrade\";"
                      "proxy_set_header Upgrade $http_upgrade;"
                      "proxy_set_header Host $host;"
                      "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                      "proxy_set_header X-Forwarded-Proto $scheme;"
                      "proxy_read_timeout 900s;"))))))
          (nginx-server-configuration
           (listen '("443 ssl" "[::]:443 ssl"))
           (server-name '("upload.chat.joinjabber.org"))
           (ssl-certificate "/etc/letsencrypt/live/joinjabber.org/fullchain.pem")
           (ssl-certificate-key "/etc/letsencrypt/live/joinjabber.org/privkey.pem")
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("proxy_pass http://localhost:5280/;"))))))
          (nginx-server-configuration
           (listen '("443 ssl" "[::]:443 ssl"))
           (server-name '("hooks.joinjabber.org"))
           (ssl-certificate "/etc/letsencrypt/live/joinjabber.org/fullchain.pem")
           (ssl-certificate-key "/etc/letsencrypt/live/joinjabber.org/privkey.pem")
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body '("proxy_pass http://localhost:3001;"))))))))))
