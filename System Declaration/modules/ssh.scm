(define-module (modules ssh)
  #:use-module (gnu services ssh)
  #:use-module (guix gexp)
  #:export (joinjabber))

(define joinjabber
  (openssh-configuration
   ;; security improvements
   (password-authentication? #f)
   (allow-agent-forwarding? #f)
   (allow-tcp-forwarding? #f)
   ;; pam is not needed
   (use-pam? #f)
   (authorized-keys
    `(("jj"
       ,(plain-file "msavoritias.pub"
                    (format #f
                            "ssh-ed25519 ~
AAAAC3NzaC1lZDI1NTE5AAAAIEwE6oRNWbR1kuG8ujJGjgzeBEfTwHWQ8MupttjRXQci ~
JoinJabber
"))
       ,(plain-file "pep.pub"
                    (format #f "ssh-ed25519 ~
 AAAAC3NzaC1lZDI1NTE5AAAAIPtl5COhe6NC53UbqD3Gosq8mw2BKejzkpvQoyfDNAtA
")))))))
