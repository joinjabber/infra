(define-module (modules timers)
  #:use-module (guix gexp)

  #:export (garbage-collector-job))

                                        ;TODO: make these into an mcron native format. its more readable too.

                                        ; runs as root to clean the system. every Friday at 12.05
(define garbage-collector-job
  #~(job "5 12 * * 5"
         "guix gc --verify=contents,repair && guix gc --vacuum-database && guix system delete-generations 1m"))

                                        ; every Thursday at 11:10
(define prosody-certificate-import
  #~(job "10 11 * * 4"
         "prosodyctl --root cert import /etc/certs"))
