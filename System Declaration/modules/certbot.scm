(define-module (modules certbot)
  #:use-module (gnu services certbot)
  #|
  certbot is called with default options. ;
  the locatins of the certificates as per the docs are at: /etc/letsencrypt/live/ ;
  and the logs are at: /var/log/letsencrypt ;
  the way the service works it adds a block in nginx config that points all 80 domains that are defined in its config to https and to the /.well-known location. ;
  it doesn't conflict with the nginx service, because the 80 block in the nginx service, doesnt define any specific domain. ;
  |#

  #:export (joinjabber))

(define joinjabber
  (certbot-configuration
   (email "contact@joinjabber.org")
   (certificates
    (list
     (certificate-configuration
                                        ; The first domain provided will be the subject CN of the certificate, and all domains will be Subject Alternative Names on the certificate.
      (domains '("joinjabber.org" "www.joinjabber.org" "joinxmpp.org" "www.joinxmpp.org"
                 "invite.joinjabber.org" "chat.joinjabber.org" "upload.chat.joinjabber.org"
                 "hooks.joinjabber.org")))))))
