(define-module (modules networking)
  #:use-module (gnu)
  #:use-module (gnu services networking)

  #:export (joinjabber))

(define joinjabber
  (static-networking
   (addresses
    (list (network-address
           (device "eth0")
           (value "77.83.247.19/24"))
          (network-address
           (device "eth0")
           (value "2a0c:f040:0:8::3b/119"))))
   (routes
    (list (network-route
           (destination "default")
           (gateway "77.83.247.1"))
          (network-route
           (destination "default")
           (gateway "2a0c:f040:0:8::1"))))
   (name-servers '("91.221.66.59"
                   "91.221.67.46"))))
