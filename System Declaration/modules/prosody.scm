(define-module (modules prosody)
  #:use-module (gnu services messaging)
  #:export (joinjabber)
  #|
  Notes on prosody service:
  - Logs are located at `/var/log/messages` grep them for prosody
  Messages that may appear in the logs:
  -  Your configuration contains the following unreferenced components:
  This is normal. The server will work. All it means is It's a standalone muc component
  - mod_admin_socket: LuaSocket unix socket support not available or incompatible, ensure it is up to date
  This happens because LuaSocket is not updated in Guix. The Bechamel project will update it.
  - Prosody was unable to find lua-unbound
  This happens because lua-unbound is not packaged in Guix. The Bechamel project will also take care of it.
  - Use of prosodyctl start/stop/restart/reload is not recommended
  This is normal. It seems shepherd is using a different way to restart services than systemd and that is weird to prosody.
  How do certificates work? Prosody uses by default `/etc/certs/*` directory. So a symlink has been created from `/etc/certs` to the certbot default directory above.
  That way its always up to date.
  |#)


(define joinjabber
  (prosody-configuration
   #|
   There are two paths that are defined by default in prosody. ;
   - `/gnu/store/*`          ;
   Which is the path to find plugins that are packaged and installed via Guix natively. Hopefully at some point all of them will be this that we need. ;
   - `/var/lib/prosody/custom_plugins` ;
   This is the default one used by the prosodyctl installer. DO NOT add any plugins here. Prosody will complain and it is not advised upstream. ;
   Since our modules are not yet packaged by the Bechamel project a new path is defined below to symlink the modules according to https://prosody.im/doc/installing_modules#prosody-modules ;
   DO NOT clone all the modules into the plugin path because some of them may be buggy or malicious or have security flaws. ;
   |#
   (plugin-paths '("/var/lib/prosody/prosody-modules-enabled"))
   (modules-enabled (cons* "admin_shell" "posix" "server_contact_info"
                           "mam" "smacks" "bidi" "s2s_bidi" "s2s_smacks_timeout"
				           "bosh" "websocket" "report_forward"
				           %default-modules-enabled))
                                        ; These security settings need to defined specifically because otherwise they default to false.
                                        ; TODO: Remove them when the Bechamel projects updates the Prosody Service.
   (c2s-require-encryption? #t)
   (s2s-require-encryption? #t)
   (s2s-secure-auth? #t)
   (int-components
	(list
	 (int-component-configuration
	  (hostname "joinjabber.org")
	  (plugin "muc")
	  (mod-muc (mod-muc-configuration
                                        ; fully static room configuration.
                                        ; rooms are defined in default_mucs below
			    (restrict-room-creation #t)))
      (admins '("pep@bouah.net" "msavoritias@macaw.me" "kris@outmo.de"))
      (modules-enabled '("muc_mam" "slack_webhooks" "muc_webchat_url"
                         "muc_defaults" "muc_ban_ip" "muc_moderation"
                         "muc_rtbl" "vcard_muc" "muc_block_pm" "muc_limits"))
	  (raw-content "\
  muc_webchat_baseurl = \"https://chat.joinjabber.org/#/guest?join={node}\";

  muc_rtbl_jid = \"xmppbl.org\"

  default_mucs = {
    {
      jid_node = \"chat\",
      config = {
        name = \"JoinJabber General Chat\",
        description = \"Landing place for the open federated Jabber community #xmpp\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"project\",
      config = {
        name = \"JoinJabber Project Room\",
        description = \"General Workshop for JoinJabber #xmpp\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"servers\",
      config = {
        name = \"Server Operator Support\",
        description = \"Support on XMPP and related server software #xmpp #devops #techsupport\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"support\",
      config = {
        name = \"XMPP User Support\",
        description = \"Get help about your XMPP/Jabber app here :) #xmpp #techsupport\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"abuse\",
      config = {
        name = \"Abuse WG\",
        description = \"Discussing spam/harassment and other abuse problems in the Jabber/XMPP federation #xmpp\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"future\",
      config = {
        name = \"XMPP enters the future\",
        description = \"Discussions about XMPP and where it's headed #xmpp #programming\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"bridging\",
      config = {
        name = \"Bridges in XMPP discussion\",
        description = \"Gateways and bridging XMPP to other chat systems #xmpp\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"sprints\",
      config = {
        name = \"XMPP Sprints Organizing\",
        description = \"XMPP sprints organizing and discussion #xmpp\",
        persistent = true,
        public = true,
        members_only = false,
      },
    },
    {
      jid_node = \"mediation_team\",
      config = {
        name = \"Mediation Team\",
        description = \"Mediation Team\",
        persistent = true,
        logging = false,
        public = false,
        members_only = true,
        public_jids = false,
        allow_member_invites = false,
      },
    },
    {
      jid_node = \"moderation_circle\",
      config = {
        name = \"Moderation circle\",
        description = \"Room for XMPP moderators\",
        persistent = true,
        public = false,
        members_only = true,
        public_jids = false,
        allow_member_invites = true,
      },
    },
    {
      jid_node = \"cozy_tavern\",
      config = {
        name = \"Cozy Tavern\",
        description = \"Safe Space to discover and learn the XMPP network\",
        persistent = true,
        public = false,
        members_only = true,
        public_jids = false,
        allow_member_invites = true,
      },
    },
  }
"))
	 (int-component-configuration
	  (hostname "upload.chat.joinjabber.org")
	  (plugin "http_file_share"))
	 (int-component-configuration
	  (hostname "pubsub.joinjabber.org")
	  (plugin "pubsub")
	  (raw-content "\
autocreate_on_publish = true;
admins = {\"pep@bouah.net\",  \"msavoritias@macaw.me\", \"kris@outmo.de\"};
"))))
   (virtualhosts
	(list
	 (virtualhost-configuration
	  (domain "chat.joinjabber.org")
	  (authentication "anonymous")
	  (http-external-url "https://chat.joinjabber.org")
	  (raw-content "\
disco_items = { { \"joinjabber.org\", \"Chatrooms\" } }

allow_anonymous_s2s = false

http_paths = {
    bosh = \"/http-bind\";
    websocket = \"/xmpp-websocket\";
}"))))
   (raw-content "\
incoming_webhook_path = \"/msg/bnM4c3Q3YW4yOzEyXDFbM3ludWxiMTI4\"
incoming_webhook_default_nick = \"Loomio\"

consider_bosh_secure = true;
consider_websocket_secure = true;

http_ports = { 5280 };
http_interfaces = { \"127.0.0.1\", \"::1\" };
https_ports = {};
https_interfaces = {};

contact_info = {
  abuse = { \"mailto:contact@joinjabber.org\", \"xmpp:abuse@joinjabber.org?join\", \"xmpp:pep@bouah.net\", \"xmpp:msavoritias@macaw.me\", \"xmpp:kris@outmo.de\" };
  admin = { \"mailto:contact@joinjabber.org\", \"xmpp:chat@joinjabber.org?join\", \"xmpp:pep@bouah.net\", \"xmpp:msavoritias@macaw.me\", \"xmpp:kris@outmo.de\" };
}

report_forward_to = { \"abuse@joinjabber.org\" }

storage = {
  archive = \"xmlarchive\";
  muc_log = \"xmlarchive\";
}
")))
