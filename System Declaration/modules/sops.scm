(define-module (modules sops)
                                        ; to define the secret
  #:use-module (sops secrets)
                                        ; to define the service itself
  #:use-module (sops services sops)
                                        ; to canonicalize the yaml files
  #:use-module (guix utils)
                                        ; for the local file
  #:use-module (guix gexp)
  #:export (joinjabber))

                                        ; the main config file.
(define sops.yaml
                                        ; the file is one folder above current one.
  (local-file "./../.sops.yaml"
                                        ; This is because paths on the store
                                        ; can not start with dots.
              "sops.yaml"))

                                        ; the encrypted files
(define cusku.yaml
  (local-file "./../cusku.yaml"))


(define joinjabber
  (sops-service-configuration
                                        ; the age server keys that are located on the server
   (age-key-file "/home/jj/age.txt")
                                        ; the config defined above
   (config sops.yaml)
   (secrets
    (list
                                        ; the secret defined above
     (sops-secret
                                        ; the key defined in the file
                                        ; the file will be in the cusku folder, in a file named api_key that contains the secret as the only contents.
      (key '("cusku" "api_key"))
                                        ; the file the keys were defined
      (file cusku.yaml)
                                        ; this is the permissions that the file is going to have on the server
      (user "jj")
      (group "users")
      (permissions #o400))))))
