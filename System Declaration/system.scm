                                        ; Indicate which modules to import to access the functions used in this configuration.
(use-modules
 (gnu)
                                        ; to install the bootloader
 (gnu bootloader)
 (gnu bootloader grub)
                                        ; for luks mapped devices.
 (gnu system mapped-devices)
 (gnu system uuid)
 (gnu system file-systems)
                                        ; so that we can define the keyboard.
 (gnu system keyboard)
                                        ; for the unattended upgrade service
 (gnu services admin)
 (gnu services mcron)
                                        ; earlyoom services
 (gnu services linux)
                                        ; openssh
 (gnu services ssh)
                                        ; to use guix g-expressions
                                        ; like the ones in unattended upgrades
 (guix gexp)
                                        ; packages
 (gnu packages version-control)
 (gnu packages rsync)
 (gnu packages tmux)
 (gnu packages ssh)
 (gnu packages vim)
                                        ; libtree
 (gnu packages linux)
                                        ; prosody
 (gnu packages messaging)
 (ice-9 format)
                                        ; networking
 (gnu services networking)
 ((modules networking)
  #:prefix networking:)
                                        ; secrets management
 (sops secrets)
 (sops services sops)
 ((modules sops)
  #:prefix sops:)
                                        ; certbot service
 (gnu services certbot)
 ((modules certbot)
  #:prefix certbot:)
                                        ; nginx service
 (gnu services web)
 ((modules nginx)
  #:prefix nginx:)
                                        ; prosody service
 (gnu services messaging)
 ((modules prosody)
  #:prefix prosody:)
 ((modules timers)
  #:prefix timers:)
 ((modules ssh)
  #:prefix ssh:))

                                        ; Main operating system declaration.
(operating-system
 (locale "en_US.utf8")
 (timezone "Etc/UTC")
 (keyboard-layout (keyboard-layout "us,fr"))
 (host-name "anner")
 (users (cons*
         (user-account
          (name "jj")
          (group "users")
          (home-directory "/home/jj")
          (supplementary-groups '("wheel")))
         %base-user-accounts))

 (packages (append (list git
                         rsync
                         tmux
                         mosh
                         vim
                         libtree
                         mercurial
                         prosody)
                   %base-packages))

 (services
  (append
   (list
    #|
    automatic system updates. every sunday at 00.00
    it also does guix pull before so it is always the latest changes applied
    the system reconfigures the current config and doesn't reboot.
    if it fails no change happens
    |#
    (service unattended-upgrade-service-type
             (unattended-upgrade-configuration
              (operating-system-file
               (file-append (local-file "." "config-dir" #:recursive? #t)
                            "/system.scm"))))
                                        ; the garbage collector job.
    (simple-service 'my-mcron-jobs
                    mcron-service-type
                    (list timers:garbage-collector-job))
                                        ; to not make the server unresponsive when out of ram.
    (service earlyoom-service-type)
                                        ; network time
    (service ntp-service-type)
    ;; guix locate database update
    ;; happens once a week with the default channels from channels.scm
    (service package-database-service-type)
    ;; clean logs every ten days
    (service log-cleanup-service-type
             (log-cleanup-configuration
              (expiry (* 10 24 3600))
              (directory "/var/log")))
                                        ; networking
    (service static-networking-service-type
             (list networking:joinjabber))

                                        ; compressed ram.
    (service zram-device-service-type
             (zram-device-configuration
                                        ; this is double the amount of ram plus 1-2 gb
              (size "5G")
              (compression-algorithm 'zstd)
                                        ; maximum priority. so everything goes here.
              (priority 32767)))
    (service sops-secrets-service-type
             sops:joinjabber)
    (service openssh-service-type
             ssh:joinjabber)
    (service certbot-service-type
             certbot:joinjabber)
    (service nginx-service-type
             nginx:joinjabber)
    (service prosody-service-type
             prosody:joinjabber))
   %base-services))

                                        ; bootloader for the system.
 (bootloader (bootloader-configuration
                                        ; legacy boot
              (bootloader grub-bootloader)
                                        ; where to install grub.
              (targets (list "/dev/vda"))
                                        ; wait forever for a choice.
              (timeout -1)
                                        ; pull the keyboard layout we declared.
              (keyboard-layout keyboard-layout)))

                                        ; luks encryption mapping of the hard drive.
 (mapped-devices
  (list (mapped-device
                                        ; uuid of the hard drive.
         (source (uuid "bdb7ef4b-49ce-41bf-aa00-56864a680506"))
                                        ; uuid of the target btrfs partition.
         (target "bc1f4795-7459-4de1-adda-4ea0e1b78cb6")
         (type luks-device-mapping))))

 (file-systems (cons* (file-system
                                        ; this can be taken with btrfs filesystem show <mountpoint>.
                       (device (uuid "bc1f4795-7459-4de1-adda-4ea0e1b78cb6" 'btrfs))
                       (mount-point "/")
                       (type "btrfs")
                                        ; automatic defrag, compression and trimming.
                       (options "autodefrag,compress=zstd:3,discard=async")
                                        ; pull in the encrypted devices we declared above.
                       (dependencies mapped-devices))
                      %base-file-systems)))
