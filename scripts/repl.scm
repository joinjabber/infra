(define guile-repl
  (lambda ()
    #|
    Run the guile repl if emacs is not used.
    - r7rs: to use the r7rs standard
    - L: to add the current folder to the module load path
    |#
    (system* "guile" "--r7rs" "--debug" "-L" ".")))

(guile-repl)
