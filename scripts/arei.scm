(define nrepl-server
  (lambda ()
    ;; this command is specified in the emacs arei project readme to start the nrepl server in the background
    (system* "guile" "-c" "((@ (ares server) run-nrepl-server))")))

(nrepl-server)
