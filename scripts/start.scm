(define container
  (lambda ()
    #|
    To start a shell with all the needed packages for development
    - container: to isolate the shell from the rest of the system
    - nesting: to be able to run guix inside the shell
    - network: to be able to communicate with the network from inside the shell
    - manifest: to make available in the shell all the packages described in the manifest
    |#
    (system* "guix" "shell" "--container" "--nesting" "--network" "--manifest=manifest.scm")))

(container)
