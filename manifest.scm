                                        ; root packages module
(use-modules (gnu packages)
             ;; guile package
             (gnu packages guile)
             ;; git package
             (gnu packages version-control)
             (gnu packages gnupg)
             ;; arei
             (gnu packages guile-xyz)
             (gnu packages emacs-xyz)
             (gnu packages certs))

                                        ; guile package needs to be defined with this name for the latest stable version
(packages->manifest (list guile-next
                          git
                          libgcrypt
                          nss-certs
                          coreutils
                          guile-ares-rs
                          emacs-arei))
