# Infra

JoinJabber Infrastructure built on Guix.

## Start developing

To change or add something to the JoinJabber infrastructure there are a couple of guile scripts to help in the repo.
Note: All commands assume that the guix package is present in the system. 

1. Create a new container:
``` shell
guile -s scripts/start.scm
```

2. If you are using GNU Emacs run:

``` shell
guile -s scripts/arei.scm
```

If you are not using GNU Emacs but want to test your code in the repl run:

``` shell
guile -s scripts/repl.scm
```

## Testing your changes

To test changes that were made to the system configuration run from inside the `System Declaration/` directory the commands described in the Bechamel Guix Documentation.

The commands can be found [here](https://codeberg.org/Bechamel_Guix/collective/wiki/Testing-System-changes).

## Apply the changes on the server

To apply the changes on the server, ssh into the server and do:
``` shell
cd "Infra/System Declaration/"
git pull
sudo guix system reconfigure --load-path=. system.scm
```

To restart any services:
``` shell
sudo herd restart <service you want>
```
Logs are in the `/var/logs` directory unless otherwise specified.

Note: Just restarting prosody will not work and will instead leave prosody in a half broken state. There is an issue [open for it.](https://codeberg.org/joinjabber/Infra/issues/31). Instead you need to restart the VM completely.

To check the status of a service do:
``` shell
herd status <service you want>
```

# Social Rules
## CoC
This project follows the [JoinJabber CoC](https://joinjabber.org/about/community/codeofconduct/)

## License

[AGPL3.0](https://www.gnu.org/licenses/agpl-3.0.html)

## Contact

[project@joinjabber.org](xmpp:project@joinjabber.org?join)
